# PWM

## 一、例程简介

PWM例程可控制PWM引脚占空比，并可自由开启关闭PWM引脚输出

## 二、测试环境说明

### 1）硬件准备

- 欧拉派
- 拓展板
- 舵机

### 2）软件环境

无

## 三、硬件连接

如图所示为欧拉派与拓展板连接方式

![image-20240202143911653](./README.assets/image-20240202143911653.png)

下图为舵机与拓展板连接图

![image-20240219133157350](./README.assets/image-20240219133157350.png)

## 四、例程运行说明

进入欧拉派的终端,执行pwm程序

```c
PWM_help:
./pwm <1> <2> <3> <4>
<1> be open or close to enable/disable PWM
<2> be 1 or 15 to chose PWM0_1 or PWM0_15
<3> be value for period
<3> be value for duty_cycle
```

#### 1）示例一 开启舵机

将舵机与拓展板连接，并执行如下指令

```c
./pwm open 1 20000000 2500000 
```

舵机开始转动

#### 2）示例二 关闭舵机

```c
./pwm close 1 
```

舵机停止转动

​	





