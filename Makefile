export TOP_DIR=$(CURDIR)
export SCRIPTS_DIR=$(TOP_DIR)/scripts
export OUTPUT_DIR=$(TOP_DIR)/output
export CROSS_COMPILE ?= aarch64-openeuler-linux-

export CC=$(CROSS_COMPILE)gcc

targets = $(shell find -maxdepth 1 -type d ! -name ".*" | grep -v "scripts\|output\|Licenses\|tmp\|mpp")

build:
	echo "CROSS_COMPILE:$(CROSS_COMPILE)"
	@for dir in $(targets); do \
		if [ -f $(TOP_DIR)/$$dir/Makefile ]; then \
			cd $(TOP_DIR)/$$dir && $(MAKE); \
		fi; \
	done

$(targets):
	echo "CROSS_COMPILE:$(CROSS_COMPILE)"
	cd $(TOP_DIR)/$@ && $(MAKE);

clean:
	@for dir in $(targets); do \
		if [ -f $(TOP_DIR)/$$dir/Makefile ]; then \
			cd $(TOP_DIR)/$$dir && $(MAKE) clean; \
		fi; \
	done
	rm -rf $(OUTPUT_DIR)

.PHONY:build clean $(targets)
