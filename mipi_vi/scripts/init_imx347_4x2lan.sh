#!/bin/sh

bspmm 0x11018440 0x8003
bspmm 0x11018460 0x8003 
bspmm 0x11018480 0x8003 
bspmm 0x110184A0 0x8003 

#GPIO9_5
bspmm 0x0102F00F0 0x1201
echo 77 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio77/direction
echo 1 > /sys/class/gpio/gpio77/value
echo 0 > /sys/class/gpio/gpio77/value
echo 1 > /sys/class/gpio/gpio77/value

#GPIO9_6
bspmm 0x0102F00F4 0x1201
echo 78 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio78/direction
echo 1 > /sys/class/gpio/gpio78/value
echo 0 > /sys/class/gpio/gpio78/value
echo 1 > /sys/class/gpio/gpio78/value