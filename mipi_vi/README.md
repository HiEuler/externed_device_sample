# sample_vio

## 一、例程简介

sample_vio例程实现从mipi接口摄像头读取图像数据并输出到HDMI屏幕

## 二、测试环境说明

### 1) 硬件准备

- 欧拉派
- 四路摄像头扩展版
- HDMI屏幕（支持1080p@30）

### 2) 软件环境

无

## 三、硬件连接

将欧拉派、摄像头、HDMI按下图方式接线。

<img src="./README.assets/57ca164ddfcb1d2411ca40caa330cd00.png" alt="57ca164ddfcb1d2411ca40caa330cd00" style="zoom:67%;" />

## 四、例程运行说明

vio中有其它许多选项，本次使用选项7测试4路IMX347摄像头

当前例程在板端`/root/device_sample/mipi_vi`目录下，以下命令需切换到该目录下执行。

```txt
usage : ./sample_vio <index>
index:
os08a20 24M 1080P60:
    (0) one sensor(i2c-5)         :vi one sensor (offline) -> vpss -> venc && vo.
    (1) one sensor(i2c-7)         :vi one sensor (offline) -> vpss -> venc && vo.
    (2) two sensor                :vi two sensor (offline) -> vpss -> venc && vo.
os08a20 24M 4K30:
    (3) one sensor(i2c-5)         :vi one sensor (offline) -> vpss -> venc && vo.
    (4) one sensor(i2c-7)         :vi one sensor (offline) -> vpss -> venc && vo.
    (5) two sensor                :vi two sensor (offline) -> vpss -> venc && vo.
imx347 37M 1080P30:
    (6) one sensor(i2c-7)         :vi one sensor (offline) -> vpss -> venc && vo.
    (7) four sensor               :vi one sensor (offline) -> vpss -> venc && vo.
imx485 37M 1080P60:
    (8) one sensor(i2c-5)         :vi one sensor (offline) -> vpss -> venc && vo.
    (9) one sensor(i2c-7)         :vi one sensor (offline) -> vpss -> venc && vo.
imx485 37M 4K30:
    (10) one sensor(i2c-5)         :vi one sensor (offline) -> vpss -> venc && vo.
    (11) one sensor(i2c-7)         :vi one sensor (offline) -> vpss -> venc && vo.
```

### 1) 初始化

sensor运行需要进行复位和启动时钟等操作。执行下面的脚本完成摄像头的初始化

```shell
./scripts/init_imx347_4x2lan.sh
```

### 2) 例程运行

```shell
./sample_vio 7
```

执行完成后系统打印如下输出

![image-20240206140841682](./README.assets/image-20240206140841682.png)

HDMI屏幕显示如下画面

![img_v3_027q_5f91558b-d83c-4b83-86e2-1b9b16165cdg](./README.assets/img_v3_027q_5f91558b-d83c-4b83-86e2-1b9b16165cdg.jpg)





