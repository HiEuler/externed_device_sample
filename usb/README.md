# USB

## 一 例程简介

USB例程为USB存储单元（U盘）测试脚本，该脚本可测试usb设备读写速度，同时也支持其他存储设备测试读写速率，如sd卡

## 二 测试环境说明

### 1）硬件准备

- 欧拉派
- U盘（空盘）

#### 2）软件环境

无

## 三 硬件连接

如图所示为USB3.0接口

![image-20240201083938126](./README.assets/image-20240201083938126.png)

将U盘插入USB3.0接口中，如下图所示

![image-20240201084426644](./README.assets/image-20240201084426644.png)

## 四 例程运行说明

U盘插入后将会打印相关信息

![image-20240201085119570](./README.assets/image-20240201085119570.png)

执行U盘分区并格式化为ext4文件系统格式

```c
fdisk /dev/sda #注意：需要通过打印信息查看你自己的u盘节点是哪个，一般会在sd[a-z]之间，如果你的u盘节点	                 是/dev/sdb，那么就执行fdisk /dev/sdb
```

按下图红框执行即可做出一个500M的分区 /dev/sda1

![image-20240201090620293](./README.assets/image-20240201090620293.png)

```c
mkfs.ext4 /dev/sda1 # 将刚刚做好的分区格式化为ext4格式
```

![image-20240201090810278](./README.assets/image-20240201090810278.png)

__test_usb.sh 脚本用法__：

```c 
./test_usb.sh <device_node> # 参数为存储设备分区节点
```

```c
Eg:    
./test_usb.sh /dev/sda1 
```











