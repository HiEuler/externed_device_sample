# sample_audio

# 介绍

当前sample_audio是一个音频示例，可以录制音频并保存文件以及播放音频文件

# 使用说明

测试前将带麦克风的耳机接入开发板音频输入输出接口。
编译成功后，将output/audio上传到开发板/root/device_sample/audio

```
export LD_LIBRARY_PATH=/usr/lib
cd cd /root/device_sample/audio
./audio_sample <index>
index and its function list below
  0:  start AI to AO loop
  1:  send audio frame to AENC channel from AI, save them
  2:  read audio stream from file, decode and send AO
  3:  read audio stream from Mydream44100.aac file, decode and send AO
  4:  start AI(VQE process), then send to AO
```

