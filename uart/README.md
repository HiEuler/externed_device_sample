# UART

## 一、例程简介
uart例程主要基于EulerPi上面的uart4串口按照约定的格式进行发送数据

## 二、测试环境说明

### 1) 硬件准备

- 欧拉派
- 串口调试小板

### 2) 软件环境

- MabaXterm
- SSCOM

## 三、硬件连接

如图所示为uart4的硬件连接图，pin6脚为GND、pin8脚为TXD、pin10脚为RXD，对应连接串口小板的GND、RXD、TXD，即两端的TXD和RXD交叉连接。

<img src="./README.assets/image-20240130163038462.png" alt="image-20240130163038462" style="zoom:50%;" />


## 四、例程运行说明

在开始测试前先将串口小板接到PC端，并在PC端打开串口调试工具。
更改uart4复用关系：

```shell
bspmm 0x0102F0134 0x1201
bspmm 0x0102F0138 0x1201
```

### 1) 发送测试

首先将uart_senddata可执行文件挂载到板端，使用`uart_senddata`命令进行发送测试，在欧拉派的终端执行，通过uart4发送`HiEuler`

```shell
./uart_senddata /dev/ttyAMA4 9600 "HiEuler"
```
运行成功后，即可在串口调试工具看到相关消息，如下图所示：

<img src="./README.assets/image-20240130164201797.png" alt="image-20240130164201797" style="zoom:50%;" />



