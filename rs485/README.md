# RS485

## 一、例程简介
RS485例程共包含两个部分，发送数据测试和接收数据测试。其中RS485_recvdata用于测试数据接收，RS485_senddata用于测试数据发送。

## 二、测试环境说明

### 1) 硬件准备

- 欧拉派
- RS485调试工具

### 2) 软件环境

无

## 三、硬件连接

如图所示为RS485信号的接线端子，RS485 A B信号的顺序为左B右A。

<img src="./README.assets/image-20240130163038462.png" alt="image-20240130163038462" style="zoom:50%;" />

与RS485调试工具的连接如图所示，调试工具的RS485-A与欧拉派的RS485-A对应

<img src="./README.assets/image-20240130164201797.png" alt="image-20240130164201797" style="zoom:50%;" />

## 四、例程运行说明

在开始测试前将RS485调试工具接到PC端，并在PC端打开串口调试工具

### 1) 发送测试

使用`rs485_senddata`命令进行发送测试，在欧拉派的终端执行，通过RS485发送`Hello World`

```shell
./rs485_senddata /dev/ttyAMA3 115200 "Hello World"
```

发送后能在PC端的串口调试工具的软件中收到`Hello World`消息

### 2) 接收测试

使用`rs485_recvdata`命令进行接收测试，在欧拉派的终端执行

```shell
./rs485_recvdata /dev/ttyAMA3 115200
```

应用开启后持续接收RS485总线上的消息并打印到欧拉派终端，在PC端发送数据可在欧拉派的终端收到。使用`Ctrl+C`结束接收
