# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>

# define MAX_DATA 1023

char path[] = "/sys/devices/platform/soc/11080000.adc/iio:device0/";
char File_name[3][25] = {
    "in_voltage3_mean_raw",
    "in_voltage3_raw",
    "in_voltage3_scale"
};

enum adc_val {
    in_voltage3_mean_raw,
    in_voltage3_raw,
    in_voltage3_scale
};

int main (int argc, char **argv) {
    FILE* fp_raw;
    FILE* fp_scale;
    float voltage;
    float ratio;
    char* raw_data;
    char* raw_path;
    char* scale_data;
    char* scale_path;

    raw_data = (char*)malloc (4);
    scale_data = (char*)malloc (9);
    raw_path = (char*)malloc (sizeof(path)+25);
    scale_path = (char*)malloc (sizeof(path)+25);
    sprintf (raw_path, "%s%s", path, File_name[in_voltage3_raw]);
    sprintf (scale_path, "%s%s", path, File_name[in_voltage3_scale]);
    
    while (1) {
        fp_raw = fopen (raw_path, "r");
        fp_scale = fopen (scale_path, "r");
        fread (raw_data, 1, 4, fp_raw);
        fread (scale_data, 1, 9, fp_scale);
        ratio = atof (raw_data) / MAX_DATA;
        voltage = ratio * 1.8;
        printf ("voltage:%lf\n", voltage);
        fclose (fp_raw);
        fclose (fp_scale);
        sleep (1);
    }

    fclose (fp_raw);
    fclose (fp_scale);
    free (raw_path);
    free (raw_data);
    free (scale_path);
    free (scale_data);
    return 0;
}
