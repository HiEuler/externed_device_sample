/*
# Copyright (C) 2024 HiHope Open Source Organization .
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
 */

#include "pinctrl.h"
#include "i2c.h"
#include "osal_debug.h"
#include "cmsis_os2.h"
#include "ssd1306_fonts.h"
#include "ssd1306.h"
#include "app_init.h"

#include "soc_osal.h"
#include "bts_le_gap.h"
#include "errcode.h"
#include "securec.h"
#include "string.h"
#include "common_def.h"
#include "osal_debug.h"
#include "osal_task.h"
#include "bts_le_gap.h"
#include "sle_device_discovery.h"
#include "sle_connection_manager.h"
#include "sle_ssap_client.h"
#include "user_sle_client.h"

#include "gpio.h"
#include "bts_le_gap.h"
#include "errcode.h"
#include "hal_gpio.h"
#include "pwm.h"
#include "test_suite_log.h"

#define USB_RCU_KEYBOARD_REPORTER_LEN       9
#define USB_RCU_MOUSE_REPORTER_LEN          5
#define USB_RCU_CONSUMER_REPORTER_LEN       3

#define USB_HID_RCU_MAX_KEY_LENTH      6


#define SLE_MICRO_MULTINUM_ONE                      1
#define SLE_MICRO_MULTINUM_TWO                      2
#define SLE_ADDR_INDEX0                             0
#define SLE_ADDR_INDEX1                             1
#define SLE_ADDR_INDEX2                             2
#define SLE_ADDR_INDEX3                             3
#define SLE_ADDR_INDEX4                             4
#define SLE_ADDR_INDEX5                             5

#define CONFIG_SLE_MTU_LENGTH                       300

#define SLE_MTU_SIZE_DEFAULT                        CONFIG_SLE_MTU_LENGTH
#define SLE_SEEK_INTERVAL_DEFAULT                   0xa00
#define SLE_SEEK_WINDOW_DEFAULT                     0xa00
#define SLE_RCU_TASK_DELAY_MS                       1000
#define SLE_RCU_WAIT_SLE_CORE_READY_MS              5000
#define SLE_RCU_WAIT_SLE_ENABLE_MS                  2000
#define SLE_RCU_DONGLE_LOG                          "[sle rcu ws63]"

//Kconfig配置，暂时用宏定义替代
#define CONFIG_SLE_MULTICON_NUM                     1                 //连接的server数量
#define CONFIG_SLE_DONGLE_CLIENT                    CONFIG_DONGLE_B
#define CONFIG_DONGLE_A                             1
#define CONFIG_DONGLE_B                             2
// ! 要连接的server端地址，即RCU的SLE广播地址，主从双方需匹配
#define CONFIG_SLE_MULTICON_SERVER1_ADDR0           0x01
#define CONFIG_SLE_MULTICON_SERVER1_ADDR1           0x4A
#define CONFIG_SLE_MULTICON_SERVER1_ADDR2           0x10
#define CONFIG_SLE_MULTICON_SERVER1_ADDR3           0x99
#define CONFIG_SLE_MULTICON_SERVER1_ADDR4           0x4E
#define CONFIG_SLE_MULTICON_SERVER1_ADDR5           0x03
// ! client端的本地地址
// dongleA的地址配置

#if(CONFIG_SLE_DONGLE_CLIENT == CONFIG_DONGLE_A)
#define CONFIG_SLE_MULTICON_CLIENT_ADDR0            0x17
#define CONFIG_SLE_MULTICON_CLIENT_ADDR1            0x66
#define CONFIG_SLE_MULTICON_CLIENT_ADDR2            0x00
#define CONFIG_SLE_MULTICON_CLIENT_ADDR3            0x5B
#define CONFIG_SLE_MULTICON_CLIENT_ADDR4            0x03
#define CONFIG_SLE_MULTICON_CLIENT_ADDR5            0x4F

// dongleB的地址配置
#elif(CONFIG_SLE_DONGLE_CLIENT == CONFIG_DONGLE_B)
#define CONFIG_SLE_MULTICON_CLIENT_ADDR0            0xb0
#define CONFIG_SLE_MULTICON_CLIENT_ADDR1            0x66
#define CONFIG_SLE_MULTICON_CLIENT_ADDR2            0x00
#define CONFIG_SLE_MULTICON_CLIENT_ADDR3            0x00
#define CONFIG_SLE_MULTICON_CLIENT_ADDR4            0x7A
#define CONFIG_SLE_MULTICON_CLIENT_ADDR5            0x00
#endif

//#define SLE_MSG_MAX_SIZE 1024 /* msg element size of msg queue */
#define SLE_MSG_MAX_SIZE 32
//#define SLE_MSG_ID_LEN   sizeof(usb_hid_rcu_keyboard_report_t)
#define SLE_QUEUE_MAX_SIZE 32
unsigned long g_sle_osal_queue_id;

static ssapc_find_service_result_t g_sle_rcu_find_service_result = { 0 };
static sle_announce_seek_callbacks_t g_sle_rcu_seek_cbk = { 0 };
static sle_connection_callbacks_t g_sle_rcu_connect_cbk = { 0 };
static ssapc_callbacks_t g_sle_rcu_ssapc_cbk = { 0 };
static sle_addr_t g_sle_rcu_remote_addr = { 0 };
static ssapc_write_param_t g_sle_rcu_send_param = { 0 };
static uint16_t g_sle_rcu_conn_id = 0;
static uint8_t g_ssap_find_ready = 0;
static uint8_t g_ssap_connect_param_update_ready = 0;
static uint8_t g_ssap_connect_state = 0;
static uint8_t g_rcu_sle_buffer[] = {"hello sle micro server!"};

typedef struct sle_multicon_stru {
    uint8_t current_connect;
    uint8_t connected_num;
    uint8_t is_connected[CONFIG_SLE_MULTICON_NUM];
    uint16_t conn_id[CONFIG_SLE_MULTICON_NUM];
    uint8_t addr[CONFIG_SLE_MULTICON_NUM][SLE_ADDR_LEN];
} sle_multicon_stru_t;
static sle_multicon_stru_t sle_multicon_param = { 0 };

#define CONFIG_I2C_SCL_MASTER_PIN       15
#define CONFIG_I2C_SDA_MASTER_PIN       16
#define CONFIG_I2C_MASTER_PIN_MODE      2
#define I2C_MASTER_ADDR                 0x0
#define I2C_SLAVE1_ADDR                 0x38
#define I2C_SET_BANDRATE                400000



#define CONFIG_BLINKY_DURATION_MS   10
#define CONFIG_GPIO7_PIN             7 // RED
#define CONFIG_GPIO11_PIN            11 // GREEN
#define CONFIG_GPIO10_PIN            10 // YELLOW


void gpio_set_value(pin_t pin)
{
    uapi_pin_set_mode(pin, HAL_PIO_FUNC_GPIO);
    uapi_gpio_set_dir(pin, GPIO_DIRECTION_OUTPUT);
    uapi_gpio_set_val(pin, GPIO_LEVEL_HIGH);
}

static void led_red(void)
{
    //red on
 //   gpio_set_value(CONFIG_GPIO7_PIN);
    uapi_pin_set_mode(CONFIG_GPIO7_PIN, HAL_PIO_FUNC_GPIO);
    uapi_gpio_set_dir(CONFIG_GPIO7_PIN, GPIO_DIRECTION_OUTPUT);
    uapi_gpio_set_val(CONFIG_GPIO7_PIN, GPIO_LEVEL_HIGH);
	//green off 
    uapi_pin_set_mode(CONFIG_GPIO11_PIN, HAL_PIO_FUNC_GPIO);
    uapi_gpio_set_dir(CONFIG_GPIO11_PIN, GPIO_DIRECTION_OUTPUT);
    uapi_gpio_set_val(CONFIG_GPIO11_PIN, GPIO_LEVEL_LOW);
	//yellow off 
    uapi_pin_set_mode(CONFIG_GPIO10_PIN, HAL_PIO_FUNC_GPIO);
    uapi_gpio_set_dir(CONFIG_GPIO10_PIN, GPIO_DIRECTION_OUTPUT);
    uapi_gpio_set_val(CONFIG_GPIO10_PIN, GPIO_LEVEL_LOW);
	
	/* uapi_gpio_toggle(CONFIG_GPIO7_PIN);
     uapi_gpio_toggle(CONFIG_GPIO11_PIN);
     uapi_gpio_toggle(CONFIG_GPIO10_PIN);*/
}

static void led_green(void)
{
    //green on
    uapi_pin_set_mode(CONFIG_GPIO11_PIN, HAL_PIO_FUNC_GPIO);
    uapi_gpio_set_dir(CONFIG_GPIO11_PIN, GPIO_DIRECTION_OUTPUT);
    uapi_gpio_set_val(CONFIG_GPIO11_PIN, GPIO_LEVEL_HIGH);
    //gpio_set_value(CONFIG_GPIO11_PIN);
	//red off 
    uapi_pin_set_mode(CONFIG_GPIO7_PIN, HAL_PIO_FUNC_GPIO);
    uapi_gpio_set_dir(CONFIG_GPIO7_PIN, GPIO_DIRECTION_OUTPUT);
    uapi_gpio_set_val(CONFIG_GPIO7_PIN, GPIO_LEVEL_LOW);
	//yellow off 
    uapi_pin_set_mode(CONFIG_GPIO10_PIN, HAL_PIO_FUNC_GPIO);
    uapi_gpio_set_dir(CONFIG_GPIO10_PIN, GPIO_DIRECTION_OUTPUT);
    uapi_gpio_set_val(CONFIG_GPIO10_PIN, GPIO_LEVEL_LOW);
	 /*uapi_gpio_toggle(CONFIG_GPIO7_PIN);
     uapi_gpio_toggle(CONFIG_GPIO11_PIN);
     uapi_gpio_toggle(CONFIG_GPIO10_PIN);*/
}

static void led_blue(void)
{
    //green on
    uapi_pin_set_mode(CONFIG_GPIO11_PIN, HAL_PIO_FUNC_GPIO);
    uapi_gpio_set_dir(CONFIG_GPIO11_PIN, GPIO_DIRECTION_OUTPUT);
    uapi_gpio_set_val(CONFIG_GPIO11_PIN, GPIO_LEVEL_LOW);
    //gpio_set_value(CONFIG_GPIO11_PIN);
	//red off 
    uapi_pin_set_mode(CONFIG_GPIO7_PIN, HAL_PIO_FUNC_GPIO);
    uapi_gpio_set_dir(CONFIG_GPIO7_PIN, GPIO_DIRECTION_OUTPUT);
    uapi_gpio_set_val(CONFIG_GPIO7_PIN, GPIO_LEVEL_LOW);
	//yellow off 
    uapi_pin_set_mode(CONFIG_GPIO10_PIN, HAL_PIO_FUNC_GPIO);
    uapi_gpio_set_dir(CONFIG_GPIO10_PIN, GPIO_DIRECTION_OUTPUT);
    uapi_gpio_set_val(CONFIG_GPIO10_PIN, GPIO_LEVEL_HIGH);
	 /*uapi_gpio_toggle(CONFIG_GPIO7_PIN);
     uapi_gpio_toggle(CONFIG_GPIO11_PIN);
     uapi_gpio_toggle(CONFIG_GPIO10_PIN);*/
}
void app_i2c_init_pin(void)
{
    uapi_pin_set_mode(CONFIG_I2C_SCL_MASTER_PIN, CONFIG_I2C_MASTER_PIN_MODE);
    uapi_pin_set_mode(CONFIG_I2C_SDA_MASTER_PIN, CONFIG_I2C_MASTER_PIN_MODE);
}

void Oledint(void)
{
    uint32_t baudrate = I2C_SET_BANDRATE;
    uint32_t hscode = I2C_MASTER_ADDR;
    app_i2c_init_pin();
	printf("Oledint\r\n");
    errcode_t ret = uapi_i2c_master_init(1, baudrate, hscode);
    if (ret != 0) {
        printf("i2c init failed, ret = %0x\r\n", ret);
    }
    osDelay(3);
    ssd1306_Init();
    ssd1306_Fill(Black);
    ssd1306_SetCursor(8, 26);
    ssd1306_DrawString("Hello NearLink !", Font_7x10, White);
    ssd1306_UpdateScreen();
    printf("oled_dispaly_thread start\r\n");
    
}

uint16_t get_sle_rcu_conn_id(void)
{
    return g_sle_rcu_conn_id;
}

uint8_t get_sle_rcu_get_connect_state(void)
{
    return g_ssap_connect_state;
}

ssapc_write_param_t *get_sle_rcu_send_param(void)
{
    return &g_sle_rcu_send_param;
}

uint8_t get_ssap_find_ready(void)
{
    return g_ssap_find_ready;
}
uint8_t get_ssap_connect_param_update_ready(void)
{
    return g_ssap_connect_param_update_ready;
}

void sle_rcu_start_scan(void)
{
    sle_seek_param_t param = { 0 };
    param.own_addr_type = 0;
    param.filter_duplicates = 0;
    param.seek_filter_policy = 0;
    param.seek_phys = 1;
    param.seek_type[0] = 0;
    param.seek_interval[0] = SLE_SEEK_INTERVAL_DEFAULT;
    param.seek_window[0] = SLE_SEEK_WINDOW_DEFAULT;
    sle_set_seek_param(&param);
    sle_start_seek();
    // osal_msleep(SLE_RCU_TASK_DELAY_MS);
}

static void sle_rcu_client_sample_sle_enable_cbk(errcode_t status)
{
errcode_t ret;
 uint8_t local_addr[SLE_ADDR_LEN] = { CONFIG_SLE_MULTICON_CLIENT_ADDR0, CONFIG_SLE_MULTICON_CLIENT_ADDR1,
                                         CONFIG_SLE_MULTICON_CLIENT_ADDR2, CONFIG_SLE_MULTICON_CLIENT_ADDR3,
                                         CONFIG_SLE_MULTICON_CLIENT_ADDR4, CONFIG_SLE_MULTICON_CLIENT_ADDR5 };
     sle_addr_t local_address;
    local_address.type = 0;
    memcpy_s(local_address.addr, SLE_ADDR_LEN, local_addr, SLE_ADDR_LEN);


   // sle_client_tartget_server_init();
    if (status != 0) {
        osal_printk("%s sle_rcu_client_sample_sle_enable_cbk,status error\r\n", SLE_RCU_DONGLE_LOG);
    } else {
        osal_printk("%s enter callback of sle enable,start scan!\r\n", SLE_RCU_DONGLE_LOG);
		ret = sle_set_local_addr(&local_address);
		osal_printk("%s sle_set_local_addr, ret:%0x!\r\n", SLE_RCU_DONGLE_LOG,ret);
        sle_rcu_start_scan();
    }
}

static void sle_rcu_client_sample_seek_enable_cbk(errcode_t status)
{
    if (status != 0) {
        osal_printk("%s sle_rcu_client_sample_seek_enable_cbk,status error\r\n", SLE_RCU_DONGLE_LOG);
    }
}

static uint8_t sle_rcu_find_connected_server_by_addr(const uint8_t *server_addr)
{
    uint8_t i;
    for (i = 0; i < CONFIG_SLE_MULTICON_NUM; i++) {
        if (sle_multicon_param.is_connected[i] == 0) {
            continue;
        }
        if (memcmp(server_addr, sle_multicon_param.addr[i], SLE_ADDR_LEN) == 0) {
            return i;
        }
    }
    return i;
}

static void sle_rcu_client_sample_seek_result_info_cbk(sle_seek_result_info_t *seek_result_data)
{
    uint8_t find_connect_server = 0;
    osal_printk("%s sle rcu scan data :%s\r\n", SLE_RCU_DONGLE_LOG, seek_result_data->data);
    if (seek_result_data == NULL || seek_result_data->data == NULL) {
        osal_printk("seek_result_data error!\r\n");
        return;
    }
    osal_printk("%s sle rcu scan data :%s\r\n", SLE_RCU_DONGLE_LOG, seek_result_data->data);
#if 0

    if ((find_connect_server = sle_rcu_find_unconnect_server_by_addr(seek_result_data->addr.addr))
        > CONFIG_SLE_MULTICON_NUM) {
        osal_printk("%s find server addr:[0x%02x:0x%02x:xx:xx:xx:0x%02x], index = %d\r\n", SLE_RCU_DONGLE_LOG,
                    seek_result_data->addr.addr[SLE_ADDR_INDEX0], seek_result_data->addr.addr[SLE_ADDR_INDEX1],
                    seek_result_data->addr.addr[SLE_ADDR_INDEX5], find_connect_server);
        sle_multicon_param.current_connect = find_connect_server;
        (void)memcpy_s(&g_sle_rcu_remote_addr, sizeof(sle_addr_t), &seek_result_data->addr, sizeof(sle_addr_t));
        sle_stop_seek();
        // osal_msleep(SLE_RCU_TASK_DELAY_MS);
    }

#endif
#if 1
    if (memcmp(seek_result_data->addr.addr, sle_multicon_param.addr[0], SLE_ADDR_LEN) == 0) {
          
        }
    osal_printk("%s find server addr zzzzzz:[0x%02x:0x%02x:xx:xx:xx:0x%02x], index = %d\r\n", SLE_RCU_DONGLE_LOG,
                    seek_result_data->addr.addr[SLE_ADDR_INDEX0], seek_result_data->addr.addr[SLE_ADDR_INDEX1],
                    seek_result_data->addr.addr[SLE_ADDR_INDEX5], find_connect_server);
        sle_multicon_param.current_connect = find_connect_server;
		if((seek_result_data->addr.addr[SLE_ADDR_INDEX0]==0x3c)&&(seek_result_data->addr.addr[SLE_ADDR_INDEX1]==0x4a)&&(seek_result_data->addr.addr[SLE_ADDR_INDEX2]==0x83)
			&&(seek_result_data->addr.addr[SLE_ADDR_INDEX3]==0x2f)&&(seek_result_data->addr.addr[SLE_ADDR_INDEX4]==0x4e)&&(seek_result_data->addr.addr[SLE_ADDR_INDEX5]==0x33))
		{
        (void)memcpy_s(&g_sle_rcu_remote_addr, sizeof(sle_addr_t), &seek_result_data->addr, sizeof(sle_addr_t));
      osal_printk("sle_stop_seek begin xxx find_connect_server :%d\r\n", SLE_RCU_DONGLE_LOG, find_connect_server);
      sle_stop_seek();
        // osal_msleep(SLE_RCU_TASK_DELAY_MS);
     osal_printk("sle_stop_seek end yyyfind_connect_server :%d\r\n", SLE_RCU_DONGLE_LOG, find_connect_server);
	    }
#endif
}

static void sle_rcu_client_sample_seek_disable_cbk(errcode_t status)
{
    if (status != 0) {
        osal_printk("%s sle_rcu_client_sample_seek_disable_cbk,status error\r\n", SLE_RCU_DONGLE_LOG);
    } else {
        sle_connect_remote_device(&g_sle_rcu_remote_addr);
        // osal_msleep(SLE_RCU_TASK_DELAY_MS);
    }
}

static void sle_rcu_client_sample_seek_cbk_register(void)
{
    g_sle_rcu_seek_cbk.sle_enable_cb = sle_rcu_client_sample_sle_enable_cbk;
    g_sle_rcu_seek_cbk.seek_enable_cb = sle_rcu_client_sample_seek_enable_cbk;
    g_sle_rcu_seek_cbk.seek_result_cb = sle_rcu_client_sample_seek_result_info_cbk;
    g_sle_rcu_seek_cbk.seek_disable_cb = sle_rcu_client_sample_seek_disable_cbk;
    sle_announce_seek_register_callbacks(&g_sle_rcu_seek_cbk);
}

static void sle_rcu_client_sample_connect_state_changed_cbk(uint16_t conn_id, const sle_addr_t *addr,
                                                            sle_acb_state_t conn_state,
                                                            sle_pair_state_t pair_state,
                                                            sle_disc_reason_t disc_reason)
{
    unused(addr);
    unused(pair_state);
    osal_printk("%s conn state changed,connect_state:%d, disc_reason:%d\r\n", SLE_RCU_DONGLE_LOG,
                conn_state, disc_reason);
    osal_printk("%s conn state changed addr:0x%02x:xx:xx:xx:0x%02x:0x%02x\r\n", SLE_RCU_DONGLE_LOG,
                addr->addr[SLE_ADDR_INDEX0], addr->addr[SLE_ADDR_INDEX4], addr->addr[SLE_ADDR_INDEX5]);
    g_sle_rcu_conn_id = conn_id;
    if (conn_state == SLE_ACB_STATE_CONNECTED) {
        osal_printk("%s SLE_ACB_STATE_CONNECTED\r\n", SLE_RCU_DONGLE_LOG);
        g_ssap_connect_state = 1;
        ssap_exchange_info_t info = { 0 };
        info.mtu_size = SLE_MTU_SIZE_DEFAULT;
        info.version = 1;
        ssapc_exchange_info_req(0, conn_id, &info);
		led_green();
    	ssd1306_Fill(Black);
        ssd1306_SetCursor(0,0);
        ssd1306_DrawString("NearLink_DK_WS63E:", Font_7x10, White);
        //ssd1306_UpdateScreen();

        ssd1306_SetCursor(0,16);
        ssd1306_DrawString("SLE Remote", Font_7x10, White);

        ssd1306_SetCursor(0,32);
        ssd1306_DrawString("Connected !", Font_7x10, White);
        ssd1306_UpdateScreen();

        sle_multicon_param.is_connected[sle_multicon_param.current_connect] = 1;
        sle_multicon_param.connected_num++;
        if (sle_multicon_param.connected_num < CONFIG_SLE_MULTICON_NUM) {
            sle_rcu_start_scan();
        }
    } else if (conn_state == SLE_ACB_STATE_NONE) {
        osal_printk("%s SLE_ACB_STATE_NONE\r\n", SLE_RCU_DONGLE_LOG);
    } else if (conn_state == SLE_ACB_STATE_DISCONNECTED) {
        osal_printk("%s SLE_ACB_STATE_DISCONNECTED\r\n", SLE_RCU_DONGLE_LOG);
        g_ssap_connect_state = 0;
        uint8_t connected_server_id = 0;
        if ((connected_server_id = sle_rcu_find_connected_server_by_addr(addr->addr)) < CONFIG_SLE_MULTICON_NUM) {
            osal_printk("%s disconneted addr:[0x%02x:0x%02x:xx:xx:xx:0x%02x],find connected server index =%d\r\n",
                        SLE_RCU_DONGLE_LOG, sle_multicon_param.addr[connected_server_id][SLE_ADDR_INDEX0],
                        sle_multicon_param.addr[connected_server_id][SLE_ADDR_INDEX1],
                        sle_multicon_param.addr[connected_server_id][SLE_ADDR_INDEX5], connected_server_id);
            sle_multicon_param.is_connected[connected_server_id] = 0;
            sle_multicon_param.connected_num--;
        }
        if (sle_multicon_param.connected_num < CONFIG_SLE_MULTICON_NUM) {
            sle_rcu_start_scan();
        }
    } else {
        osal_printk("%s status error\r\n", SLE_RCU_DONGLE_LOG);
    }
}

static void sle_rcu_client_param_update_req_cbk(uint16_t conn_id, errcode_t status,
                                                const sle_connection_param_update_req_t *param)
{
    unused(conn_id);
    unused(param);
    g_ssap_connect_param_update_ready = 1;
    osal_printk("%s sle_micro_client_param_update_req_cbk callback!, status:%d\r\n", SLE_RCU_DONGLE_LOG, status);
}

static void sle_rcu_client_sample_connect_cbk_register(void)
{
    g_sle_rcu_connect_cbk.connect_state_changed_cb = sle_rcu_client_sample_connect_state_changed_cbk;
    g_sle_rcu_connect_cbk.connect_param_update_req_cb =  sle_rcu_client_param_update_req_cbk;
    sle_connection_register_callbacks(&g_sle_rcu_connect_cbk);
   
}

static void sle_rcu_client_sample_exchange_info_cbk(uint8_t client_id, uint16_t conn_id,
                                                    ssap_exchange_info_t *param, errcode_t status)
{
    osal_printk("%s exchange_info_cbk,pair complete client id:%d status:%d\r\n", SLE_RCU_DONGLE_LOG,
                client_id, status);
    osal_printk("%s exchange mtu, mtu size: %d, version: %d.\r\n", SLE_RCU_DONGLE_LOG,
                param->mtu_size, param->version);
    ssapc_find_structure_param_t find_param = { 0 };
    find_param.type = SSAP_FIND_TYPE_PROPERTY;
    find_param.start_hdl = 1;
    find_param.end_hdl = 0xFFFF;
    ssapc_find_structure(0, conn_id, &find_param);
    // osal_msleep(SLE_RCU_TASK_DELAY_MS);
}

static void sle_rcu_client_sample_find_structure_cbk(uint8_t client_id, uint16_t conn_id,
                                                     ssapc_find_service_result_t *service, errcode_t status)
{
    osal_printk("%s find structure cbk client: %d conn_id:%d status: %d \r\n", SLE_RCU_DONGLE_LOG,
                client_id, conn_id, status);
    osal_printk("%s find structure start_hdl:[0x%02x], end_hdl:[0x%02x], uuid len:%d\r\n", SLE_RCU_DONGLE_LOG,
                service->start_hdl, service->end_hdl, service->uuid.len);
    g_sle_rcu_find_service_result.start_hdl = service->start_hdl;
    g_sle_rcu_find_service_result.end_hdl = service->end_hdl;
    memcpy_s(&g_sle_rcu_find_service_result.uuid, sizeof(sle_uuid_t), &service->uuid, sizeof(sle_uuid_t));
}

static void sle_rcu_client_sample_find_property_cbk(uint8_t client_id, uint16_t conn_id,
                                                    ssapc_find_property_result_t *property, errcode_t status)
{
    osal_printk("%s sle_rcu_client_sample_find_property_cbk, client id: %d, conn id: %d, operate ind: %d, "
                "descriptors count: %d status:%d property->handle %d\r\n", SLE_RCU_DONGLE_LOG,
                client_id, conn_id, property->operate_indication,
                property->descriptors_count, status, property->handle);
    g_sle_rcu_send_param.handle = property->handle;
    g_sle_rcu_send_param.type = SSAP_PROPERTY_TYPE_VALUE;
}

static void sle_rcu_client_sample_find_structure_cmp_cbk(uint8_t client_id, uint16_t conn_id,
                                                         ssapc_find_structure_result_t *structure_result,
                                                         errcode_t status)
{
    unused(conn_id);
    osal_printk("%s sle_rcu_client_sample_find_structure_cmp_cbk,client id:%d status:%d type:%d uuid len:%d \r\n",
                SLE_RCU_DONGLE_LOG, client_id, status, structure_result->type, structure_result->uuid.len);
    g_ssap_find_ready = 1;
    sle_connection_param_update_t params = { 0 };
    params.conn_id = conn_id;
    params.interval_min = 0x5A;  // 4 --- 0.5ms
    params.interval_max = 0x5A;
    params.max_latency = 0;
    params.supervision_timeout = 0xC8;
    sle_update_connect_param(&params);
    osal_printk("sle_update_connect_param\r\n");
}

static void sle_rcu_client_sample_ssapc_cbk_register(ssapc_notification_callback notification_cb,
                                                     ssapc_notification_callback indication_cb)
{
    g_sle_rcu_ssapc_cbk.exchange_info_cb = sle_rcu_client_sample_exchange_info_cbk;
    g_sle_rcu_ssapc_cbk.find_structure_cb = sle_rcu_client_sample_find_structure_cbk;
    g_sle_rcu_ssapc_cbk.ssapc_find_property_cbk = sle_rcu_client_sample_find_property_cbk;
    g_sle_rcu_ssapc_cbk.find_structure_cmp_cb = sle_rcu_client_sample_find_structure_cmp_cbk;
    g_sle_rcu_ssapc_cbk.notification_cb = notification_cb;
    g_sle_rcu_ssapc_cbk.indication_cb = indication_cb;
    ssapc_register_callbacks(&g_sle_rcu_ssapc_cbk);
}

void ble_enable_cb(errcode_t status)
{
    osal_printk("%s enable status: %d\n", SLE_RCU_DONGLE_LOG, status);
}

void bt_core_enable_cbk_register(void)
{
    errcode_t ret;
    gap_ble_callbacks_t gap_cb = {0};
    gap_cb.ble_enable_cb = ble_enable_cb;
    ret = gap_ble_register_callbacks(&gap_cb);
    if (ret != ERRCODE_BT_SUCCESS) {
        osal_printk("%s register ble_enable_cb failed\r\n", SLE_RCU_DONGLE_LOG);
    }
}
//配置要连接的server端地址
static void sle_client_tartget_server_init(void)
{
    switch (CONFIG_SLE_MULTICON_NUM) {
        case SLE_MICRO_MULTINUM_ONE:
            sle_multicon_param.addr[0][SLE_ADDR_INDEX0] = CONFIG_SLE_MULTICON_SERVER1_ADDR0;
            sle_multicon_param.addr[0][SLE_ADDR_INDEX1] = CONFIG_SLE_MULTICON_SERVER1_ADDR1;
            sle_multicon_param.addr[0][SLE_ADDR_INDEX2] = CONFIG_SLE_MULTICON_SERVER1_ADDR2;
            sle_multicon_param.addr[0][SLE_ADDR_INDEX3] = CONFIG_SLE_MULTICON_SERVER1_ADDR3;
            sle_multicon_param.addr[0][SLE_ADDR_INDEX4] = CONFIG_SLE_MULTICON_SERVER1_ADDR4;
            sle_multicon_param.addr[0][SLE_ADDR_INDEX5] = CONFIG_SLE_MULTICON_SERVER1_ADDR5;
            break;
        case SLE_MICRO_MULTINUM_TWO:
            sle_multicon_param.addr[0][SLE_ADDR_INDEX0] = CONFIG_SLE_MULTICON_SERVER1_ADDR0;
            sle_multicon_param.addr[0][SLE_ADDR_INDEX1] = CONFIG_SLE_MULTICON_SERVER1_ADDR1;
            sle_multicon_param.addr[0][SLE_ADDR_INDEX2] = CONFIG_SLE_MULTICON_SERVER1_ADDR2;
            sle_multicon_param.addr[0][SLE_ADDR_INDEX3] = CONFIG_SLE_MULTICON_SERVER1_ADDR3;
            sle_multicon_param.addr[0][SLE_ADDR_INDEX4] = CONFIG_SLE_MULTICON_SERVER1_ADDR4;
            sle_multicon_param.addr[0][SLE_ADDR_INDEX5] = CONFIG_SLE_MULTICON_SERVER1_ADDR5;
#if defined(CONFIG_SLE_EXSIT_TWO_MULTICON_SERVER)
            sle_multicon_param.addr[1][SLE_ADDR_INDEX0] = CONFIG_SLE_MULTICON_SERVER2_ADDR0;
            sle_multicon_param.addr[1][SLE_ADDR_INDEX1] = CONFIG_SLE_MULTICON_SERVER2_ADDR1;
            sle_multicon_param.addr[1][SLE_ADDR_INDEX2] = CONFIG_SLE_MULTICON_SERVER2_ADDR2;
            sle_multicon_param.addr[1][SLE_ADDR_INDEX3] = CONFIG_SLE_MULTICON_SERVER2_ADDR3;
            sle_multicon_param.addr[1][SLE_ADDR_INDEX4] = CONFIG_SLE_MULTICON_SERVER2_ADDR4;
            sle_multicon_param.addr[1][SLE_ADDR_INDEX5] = CONFIG_SLE_MULTICON_SERVER2_ADDR5;
#endif
            break;
        default:
            osal_printk("%s sle_client_tartget_server_init errro!\r\n", SLE_RCU_DONGLE_LOG);
            break;
    }
}
//sle的client端初始化，配置dongle本地地址，如需双连接，两个dongle的地址需要不一样，看看是否能使用随机数
void sle_rcu_client_init(ssapc_notification_callback notification_cb, ssapc_indication_callback indication_cb)
{
    uint8_t local_addr[SLE_ADDR_LEN] = { CONFIG_SLE_MULTICON_CLIENT_ADDR0, CONFIG_SLE_MULTICON_CLIENT_ADDR1,
                                         CONFIG_SLE_MULTICON_CLIENT_ADDR2, CONFIG_SLE_MULTICON_CLIENT_ADDR3,
                                         CONFIG_SLE_MULTICON_CLIENT_ADDR4, CONFIG_SLE_MULTICON_CLIENT_ADDR5 };
    sle_client_tartget_server_init();
    osal_msleep(SLE_RCU_WAIT_SLE_ENABLE_MS);
    bt_core_enable_cbk_register();
    // osal_msleep(SLE_RCU_WAIT_SLE_CORE_READY_MS);
    osal_msleep(3500);
    sle_addr_t local_address;
    local_address.type = 0;
    memcpy_s(local_address.addr, SLE_ADDR_LEN, local_addr, SLE_ADDR_LEN);
    //sle_set_local_addr(&local_address);
    
    sle_rcu_client_sample_seek_cbk_register();
    sle_rcu_client_sample_connect_cbk_register();
    sle_rcu_client_sample_ssapc_cbk_register(notification_cb, indication_cb);
	enable_sle();
}
#if 1
static void sle_rcu_notification_cb(uint8_t client_id, uint16_t conn_id, ssapc_handle_value_t *data, errcode_t status)
{
    unused(client_id);
    unused(conn_id);
    unused(status);
	char str[5];
    if (data == NULL || data->data_len == 0 || data->data == NULL) {
        osal_printk("%s sle_rcu_notification_cb fail, recv data is null!\r\n", SLE_RCU_DONGLE_LOG);
    }
    if (data->data_len == USB_RCU_KEYBOARD_REPORTER_LEN) {
        usb_hid_rcu_keyboard_report_t *recv_usb_hid_rcu_keyboard = NULL;
        osal_printk("%s sle rcu recive notification\r\n", SLE_RCU_DONGLE_LOG);
        recv_usb_hid_rcu_keyboard = (usb_hid_rcu_keyboard_report_t *)data->data;
        osal_printk("%s recv_usb_hid_rcu_keyboard.kind = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_keyboard->kind);
        osal_printk("%s xxx recv_usb_hid_rcu_keyboard.special_key = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_keyboard->special_key);
        osal_printk("%s recv_usb_hid_rcu_keyboard.reversed = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_keyboard->reserve);
        osal_printk("%s recv_usb_hid_rcu_keyboard.key = ", SLE_RCU_DONGLE_LOG);
        for (uint8_t i = 0; i < USB_HID_RCU_MAX_KEY_LENTH; i++) {
            osal_printk("0x%02x ", recv_usb_hid_rcu_keyboard->key[i]);
        }
        osal_printk("\r\n");

        if (recv_usb_hid_rcu_keyboard->key[0] != 0) {
            led_blue();
        }
		else {
            led_green();
		}

	ssd1306_SetCursor(32,48);
    int ret1=sprintf(str,"KeyValue:%2x",recv_usb_hid_rcu_keyboard->key[0]);
	if(ret1 < 0){
    printf("temp failed\r\n");
	}
    ssd1306_DrawString(str, Font_7x10, White);
    ssd1306_UpdateScreen();
        //sle_rcu_keyboard_dongle_send_data((usb_hid_rcu_keyboard_report_t *)data->data);
    } else if (data->data_len == USB_RCU_MOUSE_REPORTER_LEN) {
        usb_hid_rcu_mouse_report_t *recv_usb_hid_rcu_mouse = NULL;
        osal_printk("%s sle rcu recive notification\r\n", SLE_RCU_DONGLE_LOG);
        recv_usb_hid_rcu_mouse = (usb_hid_rcu_mouse_report_t *)data->data;
        osal_printk("%s recv_usb_hid_rcu_mouse.kind = [%d]\r\n", SLE_RCU_DONGLE_LOG, recv_usb_hid_rcu_mouse->kind);
        osal_printk("%s recv_usb_hid_rcu_mouse.key = [%d]\r\n", SLE_RCU_DONGLE_LOG, recv_usb_hid_rcu_mouse->key.d8);
        osal_printk("%s recv_usb_hid_rcu_mouse.x = [%d]\r\n", SLE_RCU_DONGLE_LOG, recv_usb_hid_rcu_mouse->x);
        osal_printk("%s recv_usb_hid_rcu_mouse.y = [%d]\r\n", SLE_RCU_DONGLE_LOG, recv_usb_hid_rcu_mouse->y);
        osal_printk("%s recv_usb_hid_rcu_mouse.wheel = [%d]\r\n", SLE_RCU_DONGLE_LOG, recv_usb_hid_rcu_mouse->wheel);
        //sle_rcu_mouse_dongle_send_data((usb_hid_rcu_mouse_report_t *)data->data);
    } else if (data->data_len == USB_RCU_CONSUMER_REPORTER_LEN) {
        usb_hid_rcu_consumer_report_t *recv_usb_hid_rcu_consumer = NULL;
        osal_printk("%s sle rcu recive notification\r\n", SLE_RCU_DONGLE_LOG);
        recv_usb_hid_rcu_consumer = (usb_hid_rcu_consumer_report_t *)data->data;
        osal_printk("%s recv_usb_hid_rcu_consumer.kind = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_consumer->kind);
        osal_printk("%s recv_usb_hid_rcu_consumer.comsumer_key0 = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_consumer->comsumer_key0);
        osal_printk("%s recv_usb_hid_rcu_consumer.comsumer_key1 = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_consumer->comsumer_key1);
        //sle_rcu_consumer_dongle_send_data((usb_hid_rcu_consumer_report_t *)data->data);
        if (recv_usb_hid_rcu_consumer->comsumer_key0 != 0) {
            led_blue();
        }
		else {
            led_green();
		}

	ssd1306_SetCursor(32,16);
    int ret2=sprintf(str,"keyvalue:%2x",recv_usb_hid_rcu_consumer->comsumer_key0);
	if(ret2 < 0){
    printf("temp failed\r\n");
	}
    ssd1306_DrawString(str, Font_7x10, White);
    ssd1306_UpdateScreen();
    }
}

static void sle_rcu_indication_cb(uint8_t client_id, uint16_t conn_id, ssapc_handle_value_t *data, errcode_t status)
{
    unused(client_id);
    unused(conn_id);
    unused(status);
    if (data == NULL || data->data_len == 0 || data->data == NULL) {
        osal_printk("%s sle_rcu_notification_cb fail, recv data is null!\r\n", SLE_RCU_DONGLE_LOG);
    }
    if (data->data_len == USB_RCU_KEYBOARD_REPORTER_LEN) {
        usb_hid_rcu_keyboard_report_t *recv_usb_hid_rcu_keyboard = NULL;
        osal_printk("%s sle rcu recive notification\r\n", SLE_RCU_DONGLE_LOG);
        recv_usb_hid_rcu_keyboard = (usb_hid_rcu_keyboard_report_t *)data->data;
        osal_printk("%s recv_usb_hid_rcu_keyboard.kind = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_keyboard->kind);
        osal_printk("%s recv_usb_hid_rcu_keyboard.special_key = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_keyboard->special_key);
        osal_printk("%s recv_usb_hid_rcu_keyboard.reversed = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_keyboard->reserve);
        osal_printk("%s recv_usb_hid_rcu_keyboard.key = ", SLE_RCU_DONGLE_LOG);
        for (uint8_t i = 0; i < USB_HID_RCU_MAX_KEY_LENTH; i++) {
            osal_printk("0x%02x ", recv_usb_hid_rcu_keyboard->key[i]);
        }
        osal_printk("\r\n");
        //sle_rcu_keyboard_dongle_send_data((usb_hid_rcu_keyboard_report_t *)data->data);
    } else if (data->data_len == USB_RCU_MOUSE_REPORTER_LEN) {
        usb_hid_rcu_mouse_report_t *recv_usb_hid_rcu_mouse = NULL;
        osal_printk("%s sle rcu recive notification\r\n", SLE_RCU_DONGLE_LOG);
        recv_usb_hid_rcu_mouse = (usb_hid_rcu_mouse_report_t *)data->data;
        osal_printk("%s recv_usb_hid_rcu_mouse.kind = [%d]\r\n", SLE_RCU_DONGLE_LOG, recv_usb_hid_rcu_mouse->kind);
        osal_printk("%s recv_usb_hid_rcu_mouse.key = [%d]\r\n", SLE_RCU_DONGLE_LOG, recv_usb_hid_rcu_mouse->key.d8);
        osal_printk("%s recv_usb_hid_rcu_mouse.x = [%d]\r\n", SLE_RCU_DONGLE_LOG, recv_usb_hid_rcu_mouse->x);
        osal_printk("%s recv_usb_hid_rcu_mouse.y = [%d]\r\n", SLE_RCU_DONGLE_LOG, recv_usb_hid_rcu_mouse->y);
        osal_printk("%s recv_usb_hid_rcu_mouse.wheel = [%d]\r\n", SLE_RCU_DONGLE_LOG, recv_usb_hid_rcu_mouse->wheel);
        //sle_rcu_mouse_dongle_send_data((usb_hid_rcu_mouse_report_t *)data->data);
    } else if (data->data_len == USB_RCU_CONSUMER_REPORTER_LEN) {
        usb_hid_rcu_consumer_report_t *recv_usb_hid_rcu_consumer = NULL;
        osal_printk("%s sle rcu recive notification\r\n", SLE_RCU_DONGLE_LOG);
        recv_usb_hid_rcu_consumer = (usb_hid_rcu_consumer_report_t *)data->data;
        osal_printk("%s recv_usb_hid_rcu_consumer.kind = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_consumer->kind);
        osal_printk("%s recv_usb_hid_rcu_consumer.comsumer_key0 = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_consumer->comsumer_key0);
        osal_printk("%s recv_usb_hid_rcu_consumer.comsumer_key1 = [%d]\r\n", SLE_RCU_DONGLE_LOG,
                    recv_usb_hid_rcu_consumer->comsumer_key1);
       // sle_rcu_consumer_dongle_send_data((usb_hid_rcu_consumer_report_t *)data->data);
    }
}
static void *sle_ws63_task(const char *arg)
{
    unused(arg);	
    osal_printk("%s enter sle_ws63_task\r\n", SLE_RCU_DONGLE_LOG);
    led_red();
    Oledint();
    osal_printk("%s Oledint\r\n", SLE_RCU_DONGLE_LOG);
    // 2. sle rcu client init
    sle_rcu_client_init(sle_rcu_notification_cb, sle_rcu_indication_cb);
    while (get_ssap_find_ready() == 0) {
        // sle_rcu_dongle_usb_recv_task();
        osal_msleep(500);
    }
    osal_printk("%s get_g_ssap_find_ready.\r\n", SLE_RCU_DONGLE_LOG);

    // delay for param update complete
    // osal_msleep(SLE_RCU_DONGLE_TASK_DELAY_MS);
    ssapc_write_param_t *sle_micro_send_param = get_sle_rcu_send_param();

    sle_micro_send_param->data_len = (uint8_t)strlen((char *)g_rcu_sle_buffer);
    sle_micro_send_param->data = g_rcu_sle_buffer;
#if 0
    while (1) {
        if (get_sle_rcu_get_connect_state() == 1) {
            osal_printk("%s send data to server.\r\n", SLE_RCU_DONGLE_LOG);
            ssapc_write_cmd(0, get_sle_rcu_conn_id(), sle_micro_send_param);
        }
        // osal_msleep(SLE_RCU_DONGLE_TASK_DELAY_MS);
        osal_msleep(500);
    }
#endif
    return NULL;
}

static void sle_ws63_entry(void)
{
    osal_task *task_handle = NULL;
    osal_kthread_lock();
    task_handle = osal_kthread_create((osal_kthread_handler)sle_ws63_task, 0, "WS63Task", 0x600);
    if (task_handle != NULL) {
        osal_kthread_set_priority(task_handle, 24);
        osal_kfree(task_handle);
    }
    osal_kthread_unlock();
}

/* Run the sle_ws63_entry. */
app_run(sle_ws63_entry);
#endif