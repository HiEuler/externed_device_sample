set(COMPONENT_NAME "dongle")

set(SOURCES
)

set(PUBLIC_HEADER
    ${CMAKE_CURRENT_SOURCE_DIR}
)

set(PRIVATE_HEADER
)

set(PRIVATE_DEFINES
)

set(PUBLIC_DEFINES
)

# use this when you want to add ccflags like -include xxx
set(COMPONENT_PUBLIC_CCFLAGS
)

set(COMPONENT_CCFLAGS
)

set(WHOLE_LINK
    true
)

set(MAIN_COMPONENT
    false
)



set(SOURCES_LIST
    ${CMAKE_CURRENT_SOURCE_DIR}/user_sle_client.c
    ${CMAKE_CURRENT_SOURCE_DIR}/ssd1306.c
    ${CMAKE_CURRENT_SOURCE_DIR}/ssd1306_fonts.c
)


set(SOURCES "${SOURCES}" ${SOURCES_LIST})

# install_sdk("${CMAKE_CURRENT_SOURCE_DIR}" "*")

build_component()
