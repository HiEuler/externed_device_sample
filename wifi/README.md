# wifi
连接wifi 
修改配置文件 
连接wifi需要修改配置文件： /etc/wpa_supplicant.conf
默认的配置文件内容如下：

```
ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=0
update_config=1
network={
       key_mgmt=NONE
}

```

修改后

```
ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=0
update_config=1
network={
 ssid="<wifi名>"
 psk="<wifi密码>"
}

```
启动网卡连接wifi 
【注】：如果在启动时出现以下错误，请断电重启，而不能使用复位按键


```
Authorized uses only. All activity may be monitored and reported.
sd3403 login: usb 1-1: device descriptor read/64, error -71
usb 1-1: new high-speed USB device number 3 using xhci-hcd
usb 1-1: device descriptor read/64, error -71
usb 1-1: device descriptor read/64, error -71
usb usb1-port1: attempt power cycle
usb 1-1: new high-speed USB device number 4 using xhci-hcd
usb 1-1: Device not responding to setup address.
usb 1-1: Device not responding to setup address.
usb 1-1: device not accepting address 4, error -71
usb 1-1: new high-speed USB device number 5 using xhci-hcd
usb 1-1: Device not responding to setup address.
usb 1-1: Device not responding to setup address.
usb 1-1: device not accepting address 5, error -71
usb usb1-port1: unable to enumerate USB device
```

启动wifi网卡

```
ifconfig wlan0 up
```

启动成功后出现以下日志


```
wal_netdev_open_etc,dev_name is:wlan0
plat_soc:E]pm_svc_open::not insmod service[0], return already opened.
plat_soc:E]wlan_power_open_cmd
plat_soc:E]===wlan READY==
ioctl fail
hwifi_custom_adapt_mac_device_priv_ini_param::max_bw[1].
hwifi_custom_adapt_mac_device_priv_ini_param::su bfee[1].
hwifi_custom_adapt_mac_device_priv_ini_param::ldpc[1].
hwifi_custom_adapt_mac_device_priv_ini_param::front_switch[0].
hwifi_custom_adapt_device_priv_ini_cali_mask_param::read cali_mask[8110]ret[0]
hwifi_custom_adapt_mac_device_priv_ini_param::g_uc_wlan_open_cnt[2]priv_cali_data
_up_down[0x13]
hwifi_custom_adapt_mac_device_priv_ini_param::g_uc_custom_cali_done_etc[1]auto_ca
li_mask[0x0]
hwifi_custom_adapt_device_priv_ini_param::data_len[64]
***hwifi_hcc_custom_ini_data_buf:64 ***********
***hwifi_hcc_custom_ini_data_buf:22 ***********
===hal_initialize_phy===260===
====hal_device_state_init_event=====621===21=
hwifi_get_country_code_etc already set country:CN
hwifi_get_region find CN in region_table_default
hwifi_get_region find CN in region_table_default
plat_soc:E]===wlan READY==
vap_id[0] {hmac_config_set_cus_dts_cali::cali time[3]ms ret[0]}
wifi_host_init_finish![wifi_cali1 cost 15158 ms].
=========hmac_fsm_change_state_etc state 5, addr 0x962b224 vap_id 1=========

```

连接配置文件中指定的wifi，其中-B参数指定后台执行wpa_supplicant需要一直运行，若
wpa_supplicant进程被杀死则wifi连接断开


```
wpa_supplicant -B -c /etc/wpa_supplicant.conf -i wlan0

```

连接时出现以下日志，此时wifi连接已完成


```
=========hmac_fsm_change_state_etc state 6, addr 0x960e92c vap_id 1=========
hmac_single_hal_device_scan_complete:vap[1] time[801] chan_cnt[13] chan_0[1] 
back[0] event[6] mode[0]
=========hmac_fsm_change_state_etc state 7, addr 0x960cba4 vap_id 1=========
[hmac_config_connect_etc] [2210] en_bandwidth[0] chan_number[1]
=========hmac_fsm_change_state_etc state 8, addr 0x95f7318 vap_id 1=========
=========hmac_fsm_change_state_etc state 9, addr 0x95f4f04 vap_id 1=========
=========hmac_fsm_change_state_etc state 11, addr 0x95f7ac4 vap_id 1=========
FPGA DO NOT support turbo QAM
=========hmac_fsm_change_state_etc state 12, addr 0x95f5300 vap_id 1=========
=========hmac_fsm_change_state_etc state 1, addr 0x95f68bc vap_id 1=========
[AMPDU TX] {hmac_mgmt_tx_addba_req_send::USER ID[1]TID NO[0] BAWSIZE[32] 
TXAMSDU[1].}
[AMPDU TX] hmac_mgmt_rx_addba_rsp tidno[0]         status[0] dialog_token[2] 
ba_policy[1] ba_timeout[0], baw_size[32] amsdu_supp[1] assoc_id[1]         
max_rx_ampdu_factor[3] max_ampdu_len_exp[0] protocol_mode[10] 
min_mpdu_start_spacing[4]

```

网络测试


```
# 获取IP地址(暂时无法自动设置到网卡，需要根据打印信息自行设置)
udhcpc wlan0

```


```
udhcpc: started, v1.36.1
udhcpc: broadcasting discover
udhcpc: broadcasting select for 192.168.5.54, server 192.168.5.1
udhcpc: lease of 192.168.5.54 obtained from 192.168.5.1, lease time 86400
/etc/udhcpc.d/50default: Adding DNS 114.114.114.114
/etc/udhcpc.d/50default: Adding DNS 8.8.8.8

```


```
# 设置网卡IP
ifconfig wlan0 192.168.5.54
# 测试网络
ping 192.168.5.1

```

开启热点 
修改配置文件 
开启热点需要修改配置文件： /etc/hostapd.conf
配置文件内容比较多，主要关注以下配置选项


```
# 指定开启热点的网卡
interface=wlan0
# 指定热点ssid
ssid=test
# 设置wpa模式
wpa=2
# 设置热点密码
wpa_passphrase=test1234
```


```
# 启动热点
hostapd /etc/hostapd.conf -ddd &
```
其它设备连接与断开时均有大量日志打印


































































