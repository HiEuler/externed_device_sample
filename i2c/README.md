# I2C

## 一 例程简介

i2c例程为oled显示屏例程

## 二 测试环境说明

### 1）硬件准备

- 欧拉派
- 拓展板
- oled显示屏

### 2）软件环境

无

## 三 硬件连接

如图所示为oled显示屏连接欧拉派拓展板

![image-20240219143146815](./README.assets/image-20240219143146815.png)

![image-20240218141258226](./README.assets/image-20240218141258226.png)

## 四 例程运行说明

在开始测试前需检查oled驱动节点是否存在

```c
ls /dev/oled*
```

__oled显示屏显示ETH0网卡IP__

```c
ifconfig eth0 | grep -oP 'inet addr:\K\S+' | awk '{print $1}' >> "/dev/oled-0"
```

__oled显示屏显示易百纳鲸鱼logo__

```c
./oled "/dev/oled-0" 1
```

![image-20240219135411148](./README.assets/image-20240219135411148.png)

