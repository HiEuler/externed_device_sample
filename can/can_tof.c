#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <stdint.h>
#include<time.h>

int can_init()
{
    int s;
    struct ifreq ifr;
    struct sockaddr_can addr;
    // 创建套接字
    if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
        perror("Error while opening socket");
        return -1;
    }
    // 设置CAN接口名称
    strcpy(ifr.ifr_name, "can0");
    ioctl(s, SIOCGIFINDEX, &ifr);
    // 绑定套接字到CAN接口
    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;
    if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        perror("Error in socket bind");
        return -2;
    }
    return s;
}

int tof_start(int s)
{
    struct can_frame frame;
    // 准备CAN帧
    frame.can_id = 0x402;  // 11-bit identifier
    frame.can_dlc = 8;     // Data length code
    frame.data[0] = 0xFF;
    frame.data[1] = 0xFF;
    frame.data[2] = 0xFF;
    frame.data[3] = 0x01;
    frame.data[4] = 0xFF;
    frame.data[5] = 0xFF;
    frame.data[6] = 0xFF;
    frame.data[7] = 0xFF;
    // 发送CAN帧
    if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
        printf("send can_frame fail\n");
        perror("Error in socket write");
        return -3;
    }
    return 1;
}

int tof_get(int s)
{
     struct can_frame frame;
     uint32_t data;
    // read CAN帧

    //fflush(s);
    printf("read data start !\n");
      if (read(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
        perror("Error in socket read");
        return -4;
    }

    if((frame.can_id&0x1FFFFFFF) != 513)
    {
        return 0;
    }
    
    data = (uint32_t)(frame.data[0] << 8 | frame.data[1] << 16 | frame.data[2] << 24)/ 256;
    float result = data /1000.0f;
    printf("juli is %f\n",result);

    return 1;
}

void main()
{
    int s = -1;
    s =can_init();
    while(1)
    {
        tof_start(s);
        usleep(5*1000); 
        tof_get(s);
        usleep(20*1000);
    }


}
