# sample_hdmi

# 介绍

当前sample_hdmi是解析H265格式视频文件通过HDMI接口输出到显示屏的示例

# 使用说明

测试前将显示器和开发板用HDMI线连接好。
编译成功后，将output/hdmi上传到开发板/root/device_sample/hdmi

```
export LD_LIBRARY_PATH=/usr/lib:/usr/lib64
cd /root/device_sample/hdmi
./sample_hdmi
hdmi_cmd:
   help                 list all command we provide
   q                    quit sample test
   hdmi_hdmi_force      force to hdmi output
   hdmi_dvi_force       force to enter dvi output mode
   hdmi_deepcolor       set video deepcolor mode
   hdmi_video_timing    set video output timing format
   hdmi_color_mode      set video color output(RGB/ycbcr)
   hdmi_aspectratio     set video aspectratio
   hdmi_a_freq          set audio output frequence
   hdmi_authmode        authmode enable or disable
```
