# sample_uvc

# 介绍

当前sample_uvc是一路usb摄像头输入到HDMI TX输出的示例

# 使用说明

编译文件系统后可在`/root/device_sample/`路径下找到此文件

```
sd3403 ~/device_sample # ll
total 10464
-rwxr-xr-x 1 root root 5235120 Mar  9 12:34 sample_audio
-rwxr-xr-x 1 root root 2709528 Mar  9 12:34 sample_hdmi
-rwx------ 1 root root 2747096 Mar  9 12:54 sample_uvc
drwxr-xr-x 2 root root    4096 Mar  9 12:34 source_file
-rwxr-xr-x 1 root root   10224 Mar  9 12:34 test
sd3403 ~/device_sample #
```

执行

```
sd3403 ~/device_sample # ./sample_uvc
sample_uvc_usage: ./sample_uvc device [options]
supported options:
-f, --format format             set the video format
-F, --file[=name]               write file
-h, --help                      show help info
-s, --size WxH                  set the frame size (eg. 1920x1080)

inquire USB device format: ./sample_uvc /dev/video0 --enum-formats

example of setting USB device format:
    ./sample_uvc /dev/video0 -fH264  -s1920x1080 -Ftest.h264
    ./sample_uvc /dev/video0 -fH265  -s1920x1080 -Ftest.h265
    ./sample_uvc /dev/video0 -fMJPEG -s1920x1080 -Ftest.mjpg
    ./sample_uvc /dev/video0 -fYUYV  -s1920x1080 -Ftest.yuv
    ./sample_uvc /dev/video0 -fNV21  -s640x360   -Ftest.yuv

note: set macro MEDIA_WORK to 0 to write file on disk.

sd3403 ~/device_sample #
```

查看当前有几个USB摄像头设备

```
sd3403 ~/device_sample # ll /dev/video
video0  video1
```

查询当前USB摄像头支持的视频格式

```
sd3403 ~/device_sample # ./sample_uvc /dev/video0 --enum-formats
usb 3-1.1: reset high-speed USB device number 3 using xhci-hcd
Device /dev/video0 opened.
Device `HD Pro Webcam C920' on `usb-10320000.xhci_1-1.1' (driver 'uvcvideo') supports video, capture, without mplanes.
- Available formats:
        Format 0: YUYV (56595559)
        Type: Video capture (1)
        Name: YUYV 4:2:2
        Frame size: 640x480 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 160x90 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 160x120 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 176x144 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 320x180 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 320x240 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 352x288 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 432x240 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 640x360 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 800x448 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 800x600 (1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 864x480 (1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 960x720 (1/15, 1/10, 2/15, 1/5)
        Frame size: 1024x576 (1/15, 1/10, 2/15, 1/5)
        Frame size: 1280x720 (1/10, 2/15, 1/5)
        Frame size: 1600x896 (2/15, 1/5)
        Frame size: 1920x1080 (1/5)
        Frame size: 2560x1472 (1/2)

        Format 1: MJPEG (47504a4d)
        Type: Video capture (1)
        Name: Motion-JPEG
        Frame size: 640x480 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 160x90 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 160x120 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 176x144 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 320x180 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 320x240 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 352x288 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 432x240 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 640x360 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 800x448 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 800x600 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 864x480 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 960x720 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 1024x576 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 1280x720 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 1600x896 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 1920x1080 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)

- Available inputs:
[sample_uvc_video_enum_inputs]-1811:    Input 0: Input 1.
[sample_uvc_video_enum_inputs]-1814:
video format: YUYV (56595559) 640x480 (stride 1280) field filed_none buffer size 0
Current frame rate: 1/30
Setting frame rate to: 1/25
Frame rate set: 1/24
sd3403 ~/device_sample #
```

插上HDMI显示器后根据当前摄像头支持的视频格式进行传参，可手动`ctrl+c`结束进程

```
sd3403 ~/device_sample # ./sample_uvc /dev/video0 -fMJPEG -s1280x720 -Ftest.mjpg
Device /dev/video0 opened.
Device `HD Pro Webcam C920' on `usb-10320000.xhci_1-1.1' (driver 'uvcvideo') supports video, capture, without mplanes.
- Available formats:
        Format 0: YUYV (56595559)
        Type: Video capture (1)
        Name: YUYV 4:2:2
        Frame size: 640x480 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 160x90 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 160x120 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 176x144 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 320x180 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 320x240 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 352x288 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 432x240 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 640x360 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 800x448 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 800x600 (1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 864x480 (1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 960x720 (1/15, 1/10, 2/15, 1/5)
        Frame size: 1024x576 (1/15, 1/10, 2/15, 1/5)
        Frame size: 1280x720 (1/10, 2/15, 1/5)
        Frame size: 1600x896 (2/15, 1/5)
        Frame size: 1920x1080 (1/5)
        Frame size: 2560x1472 (1/2)

        Format 1: MJPEG (47504a4d)
        Type: Video capture (1)
        Name: Motion-JPEG
        Frame size: 640x480 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 160x90 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 160x120 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 176x144 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 320x180 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 320x240 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 352x288 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 432x240 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 640x360 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 800x448 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 800x600 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 864x480 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 960x720 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 1024x576 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 1280x720 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 1600x896 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)
        Frame size: 1920x1080 (1/30, 1/24, 1/20, 1/15, 1/10, 2/15, 1/5)

- Available inputs:
[sample_uvc_video_enum_inputs]-1811:    Input 0: Input 1.
[sample_uvc_video_enum_inputs]-1814:
video format set: MJPEG (47504a4d) 1280x720 (stride 0) field filed_none buffer size 1843200
video format: MJPEG (47504a4d) 1280x720 (stride 0) field filed_none buffer size 1843200
Current frame rate: 1/30
Setting frame rate to: 1/25
Frame rate set: 1/24
8 buffers requested.
length: 1843200 offset: 0 timestamp type/source: monotonic/soe
Buffer 0/0 mapped at address 0x7f97e1e000.
length: 1843200 offset: 1843200 timestamp type/source: monotonic/soe
Buffer 1/0 mapped at address 0x7f97c5c000.
length: 1843200 offset: 3686400 timestamp type/source: monotonic/soe
Buffer 2/0 mapped at address 0x7f97a9a000.
length: 1843200 offset: 5529600 timestamp type/source: monotonic/soe
Buffer 3/0 mapped at address 0x7f978d8000.
length: 1843200 offset: 7372800 timestamp type/source: monotonic/soe
Buffer 4/0 mapped at address 0x7f97716000.
length: 1843200 offset: 9216000 timestamp type/source: monotonic/soe
Buffer 5/0 mapped at address 0x7f97554000.
length: 1843200 offset: 11059200 timestamp type/source: monotonic/soe
Buffer 6/0 mapped at address 0x7f97392000.
length: 1843200 offset: 12902400 timestamp type/source: monotonic/soe
Buffer 7/0 mapped at address 0x7f971d0000.
^C
media exit...
sd3403 ~/device_sample #
sd3403 ~/device_sample #
```

# 注意事项

当前sample可以支持一路的常见类型的USB摄像头

HDMI显示器请尝试使用低分辨率小屏或多使用其他厂家显示器测试，当前使用联想ThinkVision24寸显示器会一直提示输入格式超出范围无法出图，使用个别小屏可以正常显示